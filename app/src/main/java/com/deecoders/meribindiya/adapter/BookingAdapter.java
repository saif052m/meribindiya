package com.deecoders.meribindiya.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.deecoders.meribindiya.R;
import com.deecoders.meribindiya.activity.BookingDetails;
import com.deecoders.meribindiya.constants.Constants;
import com.deecoders.meribindiya.model.BookingModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookingAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<BookingModel> mList;
    private LayoutInflater mLayoutInflater = null;

    public BookingAdapter(Context context, ArrayList<BookingModel> list) {
        mContext = context;
        mList = list;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int pos) {
        return mList.get(pos);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        CompleteListViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.booking_item, null);
            viewHolder = new CompleteListViewHolder(v);
            v.setTag(viewHolder);
        } else {
            viewHolder = (CompleteListViewHolder) v.getTag();
        }

        final BookingModel model = mList.get(position);
        viewHolder.name.setText(model.getServices());
        viewHolder.time.setText("Booked At: " + model.getBookingdate()+", "+model.getBooking_time());
        viewHolder.price.setText(model.getTotal() + " INR");
        viewHolder.status.setText(model.getOrderstatus());
        if(model.getOrderstatus().toLowerCase().contains("new order")){
            viewHolder.status.setBackgroundResource(R.drawable.round_theme);
            viewHolder.status.setTextColor(mContext.getResources().getColor(R.color.themeColor));
        }
        else if(model.getOrderstatus().toLowerCase().contains("complete")){
            viewHolder.status.setBackgroundResource(R.drawable.round_green);
            viewHolder.status.setTextColor(mContext.getResources().getColor(R.color.green));
        }
        else if(model.getOrderstatus().toLowerCase().contains("cancel")){
            viewHolder.status.setBackgroundResource(R.drawable.round_red);
            viewHolder.status.setTextColor(mContext.getResources().getColor(R.color.red));
        }

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.clickEffect(v);
                Intent intent = new Intent(mContext, BookingDetails.class);
                Log.e("tag", "oid: "+model.getId());
                intent.putExtra("order_id", ""+model.getId());
                mContext.startActivity(intent);
            }
        });

        return v;
    }

    static class CompleteListViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.status)
        TextView status;

        public CompleteListViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
