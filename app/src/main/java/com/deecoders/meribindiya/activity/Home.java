package com.deecoders.meribindiya.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.deecoders.meribindiya.R;
import com.deecoders.meribindiya.adapter.CategoryAdapter;
import com.deecoders.meribindiya.adapter.SliderAdapter;
import com.deecoders.meribindiya.constants.Constants;
import com.deecoders.meribindiya.model.CategoryModel;
import com.deecoders.meribindiya.model.ProductModel;
import com.deecoders.meribindiya.model.SliderModel;
import com.deecoders.meribindiya.util.MyPref;
import com.deecoders.meribindiya.util.TinyDB;
import com.deecoders.meribindiya.view.ExpandableHeightGridView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Home extends AppCompatActivity {
    @BindView(R.id.gridView)
    ExpandableHeightGridView gridView;
    CategoryAdapter categoryAdapter;
    ArrayList<CategoryModel> models = new ArrayList<>();
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    ArrayList<SliderModel> sliderModels = new ArrayList<>();
    SliderAdapter sliderAdapter;
    @BindView(R.id.actionbar_textview)
    TextView actionbarTextview;
    @BindView(R.id.titleBar)
    RelativeLayout titleBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    Handler handler;
    Runnable runnable;
    int sliderPos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        ButterKnife.bind(this);

        DrawableCompat.setTint(menu.getDrawable(), ContextCompat.getColor(this, R.color.white));
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.clickEffect(v);
                drawerToggle();
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, 0);
            }
        }, 50);

        getBanners();
        getCategories();

        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        // Showing status
        if (status != ConnectionResult.SUCCESS) {
            // Google Play Services are not available
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        }

        sendFcmRequest();

        Log.e("tag", "user_id: "+MyPref.getId(this));

    }

    private void getBanners() {
        progressBar.setVisibility(View.VISIBLE);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, Constants.banners, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("tag", "" + response.toString());
                        progressBar.setVisibility(View.GONE);
                        try {
                            String status = response.getString("status");
                            if (status.equals("success")) {
                                JSONArray jsonArray = response.getJSONArray("object");
                                Type listType = new TypeToken<ArrayList<SliderModel>>(){}.getType();
                                ArrayList<SliderModel> modelsNew =  new GsonBuilder().create().fromJson(jsonArray.toString(), listType);
                                sliderModels.addAll(modelsNew);
                                sliderAdapter = new SliderAdapter(Home.this, sliderModels);
                                final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                                recyclerView.setLayoutManager(mLayoutManager);
                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                recyclerView.setAdapter(sliderAdapter);

                                final RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(Home.this) {
                                    @Override protected int getVerticalSnapPreference() {
                                        return LinearSmoothScroller.SNAP_TO_START;
                                    }
                                };

                                handler = new Handler();
                                runnable = new Runnable() {
                                    @Override
                                    public void run() {
                                        if(progressBar == null)
                                            return;

                                        smoothScroller.setTargetPosition(sliderPos);
                                        mLayoutManager.startSmoothScroll(smoothScroller);
                                        //mLayoutManager.scrollToPositionWithOffset(sliderPos, 0);
                                        sliderPos++;
                                        if(sliderPos >= sliderModels.size()){
                                            sliderPos = 0;
                                        }
                                        handler.postDelayed(runnable, 3000);
                                    }
                                };
                                handler.postDelayed(runnable, 3000);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                progressBar.setVisibility(View.GONE);
                Toast.makeText(Home.this, "Network Problem!", Toast.LENGTH_SHORT).show();
            }
        });
        Volley.newRequestQueue(this).add(req);
    }

    private void getCategories() {
        progressBar.setVisibility(View.VISIBLE);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, Constants.getCategories, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("tag", "" + response.toString());
                        progressBar.setVisibility(View.GONE);
                        try {
                            String status = response.getString("status");
                            if (status.equals("success")) {
                                JSONArray jsonArray = response.getJSONArray("object");
                                Type listType = new TypeToken<ArrayList<CategoryModel>>(){}.getType();
                                ArrayList<CategoryModel> modelsNew =  new GsonBuilder().create().fromJson(jsonArray.toString(), listType);
                                models.addAll(modelsNew);

                                categoryAdapter = new CategoryAdapter(Home.this, models);
                                gridView.setAdapter(categoryAdapter);
                                gridView.setExpanded(true);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                progressBar.setVisibility(View.GONE);
                Toast.makeText(Home.this, "Network Problem!", Toast.LENGTH_SHORT).show();
            }
        });
        Volley.newRequestQueue(this).add(req);
    }

    boolean open;

    public void openDrawer() {
        open = true;
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public void closeDrawer() {
        open = false;
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    public void drawerToggle() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            closeDrawer();
        } else {
            openDrawer();
        }
    }

    private void sendFcmRequest() {
        JSONObject object = new JSONObject();
        try {
            object.put("fcm", FirebaseInstanceId.getInstance().getToken());
            object.put("mobile", Long.parseLong(MyPref.getMobile(this)));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("tag", "sending: "+object.toString());
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, Constants.userFcm, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("tag", "fcm: "+response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(Home.this, "Network Problem!", Toast.LENGTH_SHORT).show();
            }
        });
        Volley.newRequestQueue(this).add(req);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearCart();
        if(handler != null){
            handler.removeCallbacks(runnable);
        }
    }

    private void clearCart() {
        TinyDB tinyDB = new TinyDB(this);
        tinyDB.putListObject("products", new ArrayList<ProductModel>());
    }
}
