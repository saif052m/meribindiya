package com.deecoders.meribindiya.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.deecoders.meribindiya.R;
import com.deecoders.meribindiya.constants.Constants;
import com.deecoders.meribindiya.model.BookingDetailModel;
import com.deecoders.meribindiya.model.CustomerOrderServiceModel;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookingDetails extends AppCompatActivity {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.titleTxt)
    TextView titleTxt;
    @BindView(R.id.titlePanel)
    LinearLayout titlePanel;
    @BindView(R.id.titleBar)
    LinearLayout titleBar;
    @BindView(R.id.appointmentDate)
    TextView appointmentDate;
    @BindView(R.id.duration)
    TextView duration;
    @BindView(R.id.orderId)
    TextView orderId;
    @BindView(R.id.authCode)
    TextView authCode;
    @BindView(R.id.orderStatus)
    TextView orderStatus;
    @BindView(R.id.servicesPanel)
    LinearLayout servicesPanel;
    @BindView(R.id.totalPrice)
    TextView totalPrice;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    String orderId_;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    BookingDetailModel bookingDetailModel;
    @BindView(R.id.services)
    TextView services;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_details);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        DrawableCompat.setTint(back.getDrawable(), ContextCompat.getColor(this, R.color.white));

        orderId_ = getIntent().getStringExtra("order_id");
        scrollView.setVisibility(View.GONE);
        getBookingsDetails();
    }

    public void finish(View view) {
        Constants.clickEffect(view);
        finish();
    }

    private void getBookingsDetails() {
        progressBar.setVisibility(View.VISIBLE);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, Constants.orderDetails + "/" + orderId_, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("tag", "" + response.toString());
                        if (progressBar == null)
                            return;
                        progressBar.setVisibility(View.GONE);
                        scrollView.setVisibility(View.VISIBLE);
                        try {
                            String status = response.getString("status");
                            if (status.equals("success")) {
                                JSONObject object = response.getJSONObject("object");
                                bookingDetailModel = new GsonBuilder().create().fromJson(object.toString(), BookingDetailModel.class);
                                showBookingData();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                if (progressBar == null)
                    return;
                progressBar.setVisibility(View.GONE);
                Toast.makeText(BookingDetails.this, "Network Problem!", Toast.LENGTH_SHORT).show();
            }
        });
        Volley.newRequestQueue(BookingDetails.this).add(req);
    }

    private void showBookingData() {
        appointmentDate.setText(bookingDetailModel.getBookingdate()+", "+bookingDetailModel.getBooking_time());
        duration.setText(getDuration());
        orderId.setText("" + orderId_);
        orderStatus.setText(bookingDetailModel.getOrderstatus());
        if (bookingDetailModel.getOrderstatus().toLowerCase().contains("new order")) {
            orderStatus.setBackgroundResource(R.drawable.round_theme);
            orderStatus.setTextColor(getResources().getColor(R.color.themeColor));
        } else if (bookingDetailModel.getOrderstatus().toLowerCase().contains("complete")) {
            orderStatus.setBackgroundResource(R.drawable.round_green);
            orderStatus.setTextColor(getResources().getColor(R.color.green));
        } else if (bookingDetailModel.getOrderstatus().toLowerCase().contains("cancel")) {
            orderStatus.setBackgroundResource(R.drawable.round_red);
            orderStatus.setTextColor(getResources().getColor(R.color.red));
        }
        services.setText(bookingDetailModel.getServices());
        totalPrice.setText(bookingDetailModel.getTotal() + " INR");
        address.setText(bookingDetailModel.getAddress().getAddress());
        ArrayList<CustomerOrderServiceModel> models = bookingDetailModel.getCustomerOrderServices();
        for (CustomerOrderServiceModel model : models) {
            View view = LayoutInflater.from(this).inflate(R.layout.service_item, null, false);
            TextView name = view.findViewById(R.id.name);
            TextView quantity = view.findViewById(R.id.quantity);
            TextView singlePrice = view.findViewById(R.id.singlePrice);
            TextView subTotalPrice = view.findViewById(R.id.subTotalPrice);
            name.setText(model.getServiceName());
            quantity.setText("-("+model.getQuantity()+")");
            singlePrice.setText(model.getService_amount()+" INR");
            subTotalPrice.setText(model.getTotal_service_amount()+" INR");
            servicesPanel.addView(view);
        }
    }

    private String getDuration() {
        String duration = "0 min";
        int minutes=0;
        ArrayList<CustomerOrderServiceModel> models = bookingDetailModel.getCustomerOrderServices();
        for (CustomerOrderServiceModel model : models) {
            minutes = minutes + model.getMinutes();
        }
        duration = getTimeFromSecond(minutes*60);
        return duration;
    }

    private String getTimeFromSecond (long seconds) {
        if (seconds < 60) {
            return seconds + "sec";
        } else if (seconds < 3600) {
            return "" + (seconds / 60) + "min";
        } else if (seconds < 86400) {
            return "" + (seconds / 3600) + "H";
        } else if (seconds < 604800) {
            return "" + (seconds / 86400) + "D";
        } else if (seconds < 2419200){
            return "" + (seconds / 604800) + "W";
        } else if(seconds < 29030400){
            return "" + (seconds / 2419200) + "M";
        } else {
            return "" + (seconds / 29030400) + "Y";
        }
    }
}
