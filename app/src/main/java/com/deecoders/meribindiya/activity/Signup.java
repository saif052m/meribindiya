package com.deecoders.meribindiya.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.deecoders.meribindiya.R;
import com.deecoders.meribindiya.constants.Constants;
import com.deecoders.meribindiya.network.CustomRequest;
import com.deecoders.meribindiya.network.VolleyLibrary;
import com.devspark.appmsg.AppMsg;

import org.acra.sender.HttpSender;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Signup extends AppCompatActivity {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.titleTxt)
    TextView titleTxt;
    @BindView(R.id.titlePanel)
    LinearLayout titlePanel;
    @BindView(R.id.titleBar)
    LinearLayout titleBar;
    @BindView(R.id.mobile)
    EditText mobile;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.male)
    RadioButton male;
    @BindView(R.id.female)
    RadioButton female;
    @BindView(R.id.gender)
    RadioGroup gender;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        DrawableCompat.setTint(back.getDrawable(), ContextCompat.getColor(this, R.color.white));

        mobile.setText(getIntent().getStringExtra("mobile"));
    }

    public void registerUser(View view) {
        Constants.clickEffect(view);
        if(mobile.getText().toString().isEmpty()){
            AppMsg.makeText(this, "Enter mobile number!", AppMsg.STYLE_ALERT).show();
        }
        else if(mobile.getText().toString().length() < 10){
            AppMsg.makeText(this, "Invalid mobile number!", AppMsg.STYLE_ALERT).show();
        }
        else if(name.getText().toString().isEmpty()){
            AppMsg.makeText(this, "Enter name!", AppMsg.STYLE_ALERT).show();
        }
        else if(email.getText().toString().isEmpty()){
            AppMsg.makeText(this, "Enter email!", AppMsg.STYLE_ALERT).show();
        }
        else if(!checkEmail(email.getText().toString())){
            AppMsg.makeText(this, "Invalid email!", AppMsg.STYLE_ALERT).show();
        }
        else {
            sendRequest();
        }
    }

    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile("[a-zA-Z0-9+._%-+]{1,256}" +
                "@" +
                "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
                "(" +
                "." +
                "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
                ")+");

    public static boolean checkEmail(String email){
        if(email.isEmpty())
            return false;
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    public void finish(View view) {
        Constants.clickEffect(view);
        finish();
    }

    private void sendRequest() {
        progressBar.setVisibility(View.VISIBLE);
        JSONObject object = new JSONObject();
        try {
            object.put("email", email.getText().toString());
            object.put("name", name.getText().toString());
            object.put("mobile", mobile.getText().toString());
            if(gender.getCheckedRadioButtonId() == R.id.male) {
                object.put("gender", "male");
            }
            else if(gender.getCheckedRadioButtonId() == R.id.female) {
                object.put("gender", "female");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, Constants.userRegister, object,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("tag", ""+response.toString());
                    try {
                        String status = response.getString("status");
                        String msg = response.getString("message");
                        if (status.equals("success")) {
                            JSONObject object = response.getJSONObject("object");
                            String otpCode = object.getString("otp");
                            Intent intent = new Intent(Signup.this, OtpVerification.class);
                            intent.putExtra("code", otpCode);
                            intent.putExtra("mobile", mobile.getText().toString());
                            startActivity(intent);
                            finish();
                        }
                        else{
                            AppMsg.makeText(Signup.this, ""+msg, AppMsg.STYLE_ALERT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                progressBar.setVisibility(View.GONE);
                Toast.makeText(Signup.this, "Network Problem!", Toast.LENGTH_SHORT).show();
            }
        });
        Volley.newRequestQueue(this).add(req);
    }
}
