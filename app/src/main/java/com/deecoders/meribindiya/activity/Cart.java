package com.deecoders.meribindiya.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.deecoders.meribindiya.Checksum;
import com.deecoders.meribindiya.R;
import com.deecoders.meribindiya.adapter.CartAdapter;
import com.deecoders.meribindiya.constants.Constants;
import com.deecoders.meribindiya.model.ProductModel;
import com.deecoders.meribindiya.network.CustomRequest;
import com.deecoders.meribindiya.network.VolleyLibrary;
import com.deecoders.meribindiya.util.MyPref;
import com.deecoders.meribindiya.util.TinyDB;
import com.deecoders.meribindiya.view.ExpandableHeightGridView;
import com.google.gson.GsonBuilder;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Cart extends AppCompatActivity {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.titleTxt)
    TextView titleTxt;
    @BindView(R.id.titlePanel)
    LinearLayout titlePanel;
    @BindView(R.id.titleBar)
    LinearLayout titleBar;
    @BindView(R.id.arrow)
    ImageView arrow;
    @BindView(R.id.listView)
    ExpandableHeightGridView listView;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.proceed)
    Button proceed;
    ArrayList<ProductModel> models = new ArrayList<>();
    CartAdapter cartAdapter;
    boolean isExpanded = true;
    @BindView(R.id.count)
    TextView count;
    @BindView(R.id.address)
    TextView address;
    int addressId;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.totalPrice)
    TextView totalPrice;
    @BindView(R.id.date)
    TextView orderDate;
    @BindView(R.id.time)
    BetterSpinner orderTime;
    String date, time;
    Checksum checksum;
    JSONObject checksumObject;
    @BindView(R.id.note)
    TextView note;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        DrawableCompat.setTint(arrow.getDrawable(), ContextCompat.getColor(this, R.color.black));

        TinyDB tinyDB = new TinyDB(this);
        models = tinyDB.getListObject("products", ProductModel.class);
        clearDirtyModels();
        cartAdapter = new CartAdapter(this, models);
        listView.setAdapter(cartAdapter);
        listView.setExpanded(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, 0);
            }
        }, 50);

        showServicesCount();

        orderDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker(Cart.this);
            }
        });
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, new String[]{"loading..."});
        orderTime.setAdapter(adapter);

        registerReceiver(AddToCartReceiver, new IntentFilter("cart"));

        getTimeSlots();
    }

    private void getTimeSlots() {
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> hashMap = new HashMap<>();
        String url = Constants.time_slots;
        Log.e("tag", "url: " + url);
        CustomRequest customRequest = new CustomRequest(Request.Method.GET, url, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                progressBar.setVisibility(View.GONE);
                try {
                    String status = response.getString("status");
                    if (status.equals("success")) {
                        JSONArray arr = response.getJSONArray("object");
                        String[] values = new String[arr.length()];
                        for (int i = 0; i < arr.length(); i++) {
                            values[i] = arr.getString(i);
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Cart.this, android.R.layout.simple_spinner_dropdown_item, values);
                        orderTime.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                progressBar.setVisibility(View.GONE);
                Toast.makeText(Cart.this, "Network Problem!", Toast.LENGTH_SHORT).show();
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void showDatePicker(final Context context) {
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdformat = new SimpleDateFormat(myFormat, Locale.US);
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                date = new SimpleDateFormat(myFormat).format(calendar.getTimeInMillis());
                orderDate.setText(date);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMinDate(System.currentTimeMillis());
        dialog.show();
    }

    private void showTimePicker(final Context context) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String AM_PM;
                if (selectedHour < 12) {
                    AM_PM = "AM";
                } else {
                    AM_PM = "PM";
                }
                time = selectedHour + ":" + selectedMinute + " " + AM_PM;
                orderTime.setText(time);
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private void clearDirtyModels() {
        for (int i = 0; i < models.size(); i++) {
            if (models.get(i).getCount() == 0) {
                models.remove(i);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(AddToCartReceiver);
    }

    private BroadcastReceiver AddToCartReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("tag", "onReceive");
            TinyDB tinyDB = new TinyDB(context);
            models = tinyDB.getListObject("products", ProductModel.class);
            showServicesCount();
        }
    };

    private void showServicesCount() {
        int itemCount = 0, priceCount = 0;
        for (ProductModel model : models) {
            itemCount = itemCount + model.getCount();
            priceCount = priceCount + (model.getCount() * model.getPrice());
        }
        count.setText("" + itemCount);
        totalPrice.setText("" + priceCount + " INR");
        if (models.size() == 0) {
            proceed.setAlpha(0.5f);
            note.setVisibility(View.VISIBLE);
        }
        else{
            note.setVisibility(View.GONE);
        }
    }

    public void proceed(View view) {
        Constants.clickEffect(view);
        if (models.size() == 0)
            return;

        int priceCount = 0;
        for (ProductModel model : models) {
            priceCount = priceCount + (model.getCount() * model.getPrice());
        }
        if (priceCount < 800) {
            Toast.makeText(Cart.this, "Minimum order limit is 800 INR!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (date == null) {
            Toast.makeText(Cart.this, "Please schedule date!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (orderTime.getText().toString().equals("Time")) {
            Toast.makeText(Cart.this, "Please schedule time!", Toast.LENGTH_SHORT).show();
            return;
        }
        /*try {
            Log.e("tag", "selectedDate: "+date);
            Date selectedDate = new SimpleDateFormat("dd-MM-yyyy").parse(date);
            if(selectedDate.getTime() <= System.currentTimeMillis()){
                Toast.makeText(Cart.this, "Invalid date/time!", Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }*/

        if (addressId == 0) {
            Toast.makeText(Cart.this, "Please select an address!", Toast.LENGTH_SHORT).show();
            return;
        }

        String[] arr = new String[]{"Cash on Delivery", "Credit/Debit Cards"};
        showDialog(arr);
    }

    private void showDialog(final String[] options) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Payment Method");
        builder.setSingleChoiceItems(options, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (which == 0) {
                    sendCashOrderRequest();
                } else {
                    getCheckSum();
                }
            }
        });
        builder.create();
        builder.show();
    }

    private void getCheckSum() {
        progressBar.setVisibility(View.VISIBLE);
        JSONObject object = new JSONObject();
        try {
            object.put("userId", MyPref.getId(this));
            object.put("email", MyPref.getEmail(this));
            int priceCount = 0;
            for (ProductModel model : models) {
                priceCount = priceCount + (model.getCount() * model.getPrice());
            }
            object.put("finalAmount", priceCount);
            object.put("mobile", Long.parseLong(MyPref.getMobile(this)));
            object.put("orderId", generateString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("tag", "sending: " + object.toString());
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, Constants.generateChecksum, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressBar.setVisibility(View.GONE);
                        Log.e("generate", "" + response.toString());
                        try {
                            String status = response.getString("status");
                            String msg = response.getString("message");
                            if (status.equals("success")) {
                                checksumObject = response.getJSONObject("object");
                                checksum = new GsonBuilder().create().fromJson(checksumObject.toString(), Checksum.class);
                                validateCheckSum();
                            } else {
                                Toast.makeText(Cart.this, "" + msg, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(Cart.this, "Network Problem!", Toast.LENGTH_SHORT).show();
            }
        });
        Volley.newRequestQueue(this).add(req);
    }

    private void validateCheckSum() {
        progressBar.setVisibility(View.VISIBLE);
        JSONObject object = new JSONObject();
        try {
            object.put("userId", MyPref.getId(this));
            object.put("email", MyPref.getEmail(this));
            int priceCount = 0;
            for (ProductModel model : models) {
                priceCount = priceCount + (model.getCount() * model.getPrice());
            }
            object.put("finalAmount", priceCount);
            object.put("mobile", MyPref.getMobile(this));
            object.put("orderId", generateString());
            object.put("paramMap", checksumObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("tag", "sending: " + object.toString());
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, Constants.validateChecksum, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressBar.setVisibility(View.GONE);
                        Log.e("validate", "" + response.toString());
                        try {
                            String status = response.getString("status");
                            String msg = response.getString("message");
                            if (status.equals("success")) {
                                boolean value = response.getJSONObject("object").getBoolean("value");
                                if (value) {
                                    onStartTransaction(null);
                                } else {
                                    Toast.makeText(Cart.this, "" + msg, Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(Cart.this, "" + msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(Cart.this, "Network Problem!", Toast.LENGTH_SHORT).show();
            }
        });
        Volley.newRequestQueue(this).add(req);
    }

    public void onStartTransaction(View view) {
        PaytmPGService Service = PaytmPGService.getStagingService();
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("CALLBACK_URL", "" + checksum.getCALLBACK_URL());
        paramMap.put("CHANNEL_ID", "" + checksum.getCHANNEL_ID());
        paramMap.put("CHECKSUMHASH", "" + checksum.getCHECKSUMHASH());
        paramMap.put("CUST_ID", "" + checksum.getCUST_ID());
        paramMap.put("INDUSTRY_TYPE_ID", "" + checksum.getINDUSTRY_TYPE_ID());
        paramMap.put("MID", "" + checksum.getMID());
        paramMap.put("ORDER_ID", "" + checksum.getORDER_ID());
        paramMap.put("TXN_AMOUNT", "" + checksum.getTXN_AMOUNT());
        paramMap.put("WEBSITE", "" + checksum.getWEBSITE());
        paramMap.put("EMAIL", "" + checksum.getEMAIL());
        paramMap.put("MOBILE_NO", "" + checksum.getMOBILE_NO());

        // testing
        /*paramMap.put("CALLBACK_URL", "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=ORDS86991657");
        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("CHECKSUMHASH", "GlFsxtyLE9hr4Z/vhklhAyReRyYgY+OdfKppb1z4Pl3ePk0VGDWrOH5GLiZ5QC1wblkvXyFLo6DJap2nXFgFraLTBZWhnNNV4u6RC0xVHUM=");
        paramMap.put("CUST_ID", "CUST001");
        paramMap.put("INDUSTRY_TYPE_ID", "Retail");
        paramMap.put("MID", "Meribi16353178366179");
        paramMap.put("ORDER_ID", "ORDS86991657");
        paramMap.put("TXN_AMOUNT", "1");
        paramMap.put("WEBSITE", "APPSTAGING");
        paramMap.put("EMAIL", ""+checksum.getEMAIL());
        paramMap.put("MOBILE_NO", ""+checksum.getMOBILE_NO());*/

        for (Map.Entry<String, String> entry : paramMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Log.e("tag", key + " : " + value);
        }

        PaytmOrder Order = new PaytmOrder(paramMap);
        Service.initialize(Order, null);

        Service.startPaymentTransaction(this, true, true,
                new PaytmPaymentTransactionCallback() {
                    @Override
                    public void someUIErrorOccurred(String inErrorMessage) {
                        Log.e("tag", "error: " + inErrorMessage);
                    }

					/*@Override
					public void onTransactionSuccess(Bundle inResponse) {
						// After successful transaction this method gets called.
						// // Response bundle contains the merchant response
						// parameters.
						Log.d("LOG", "Payment Transaction is successful " + inResponse);
						Toast.makeText(getApplicationContext(), "Payment Transaction is successful ", Toast.LENGTH_LONG).show();
					}

					@Override
					public void onTransactionFailure(String inErrorMessage,
							Bundle inResponse) {
						// This method gets called if transaction failed. //
						// Here in this case transaction is completed, but with
						// a failure. // Error Message describes the reason for
						// failure. // Response bundle contains the merchant
						// response parameters.
						Log.d("LOG", "Payment Transaction Failed " + inErrorMessage);
						Toast.makeText(getBaseContext(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
					}*/

                    @Override
                    public void onTransactionResponse(Bundle inResponse) {
                        progressBar.setVisibility(View.GONE);
                        Log.e("LOG", "Payment Transaction is successful " + inResponse);
                        String txnId = inResponse.getString("TXNID");
                        String txnDate = inResponse.getString("TXNDATE");
                        sendPaytmOrderRequest(txnId, txnDate);
                    }

                    @Override
                    public void networkNotAvailable() {
                        progressBar.setVisibility(View.GONE);
                        Log.e("tag", "networkNotAvailable");
                        Toast.makeText(Cart.this, "Network problem!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void clientAuthenticationFailed(String inErrorMessage) {
                        progressBar.setVisibility(View.GONE);
                        Log.e("tag", "clientAuthenticationFailed: " + inErrorMessage);
                    }

                    @Override
                    public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
                        progressBar.setVisibility(View.GONE);
                        Log.e("tag", "onErrorLoadingWebPage: " + inErrorMessage);
                    }

                    // had to be added: NOTE
                    @Override
                    public void onBackPressedCancelTransaction() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(Cart.this, "Transaction cancelled", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                        progressBar.setVisibility(View.GONE);
                        Log.e("LOG", "Payment Transaction Failed " + inErrorMessage);
                        Toast.makeText(getBaseContext(), "Payment Transaction Failed!", Toast.LENGTH_LONG).show();
                    }

                });
    }

    private void sendPaytmOrderRequest(String txnId, String txnDate) {
        progressBar.setVisibility(View.VISIBLE);
        JSONObject object = new JSONObject();
        try {
            object.put("mobile", Long.parseLong(MyPref.getMobile(this)));
            object.put("userid", Long.parseLong(MyPref.getId(this)));
            object.put("addressid", addressId);
            object.put("booking_date", date);
            object.put("booking_time", orderTime.getText().toString());
            object.put("category_id", models.get(0).getCatid());
            object.put("online_source_id", checksum.getORDER_ID());
            object.put("online_source_txndate", txnDate);
            object.put("online_source_txnid", txnId);
            object.put("payment_source_id", "");
            object.put("payment_source_txndate", "");
            object.put("payment_source_txnid", "");
            object.put("pymnt_mode", 1);
            object.put("pymnt_source", "paytm");
            object.put("utf1", "");
            object.put("utf2", "");
            object.put("utf3", "");

            JSONArray allServices = new JSONArray();
            for (ProductModel model : models) {
                JSONObject service = new JSONObject();
                service.put("quantity", model.getCount());
                service.put("serviceid", model.getId());
                allServices.put(service);
            }
            object.put("orderServiceRequests", allServices);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("tag", "sending: " + object.toString());
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, Constants.createOrder, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressBar.setVisibility(View.GONE);
                        Log.e("tag", "" + response.toString());
                        try {
                            String status = response.getString("status");
                            String msg = response.getString("message");
                            if (status.equals("success")) {
                                showAlert(Cart.this);
                            } else {
                                Toast.makeText(Cart.this, "" + msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                progressBar.setVisibility(View.GONE);
                Toast.makeText(Cart.this, "Network Problem!", Toast.LENGTH_SHORT).show();
            }
        });
        Volley.newRequestQueue(this).add(req);
    }

    private int generateString() {
        int orderId = 0;
        Random r = new Random();
        orderId = 100000000 + r.nextInt(100000000) + r.nextInt(100000000);
        return orderId;
    }

    private void sendCashOrderRequest() {
        progressBar.setVisibility(View.VISIBLE);
        JSONObject object = new JSONObject();
        try {
            object.put("mobile", Long.parseLong(MyPref.getMobile(this)));
            object.put("userid", Long.parseLong(MyPref.getId(this)));
            object.put("addressid", addressId);
            object.put("booking_date", date);
            object.put("booking_time", orderTime.getText().toString());
            object.put("category_id", models.get(0).getCatid());
            object.put("online_source_id", "");
            object.put("online_source_txndate", "");
            object.put("online_source_txnid", "");
            object.put("payment_source_id", "");
            object.put("payment_source_txndate", "");
            object.put("payment_source_txnid", "");
            object.put("pymnt_mode", 0);
            object.put("pymnt_source", "cod");
            //object.put("pymnt_mode", 1);
            //object.put("pymnt_source", "paytm");
            object.put("utf1", "");
            object.put("utf2", "");
            object.put("utf3", "");

            JSONArray allServices = new JSONArray();
            for (ProductModel model : models) {
                JSONObject service = new JSONObject();
                service.put("quantity", model.getCount());
                service.put("serviceid", model.getId());
                allServices.put(service);
            }
            object.put("orderServiceRequests", allServices);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("tag", "sending: " + object.toString());
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, Constants.createOrder, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressBar.setVisibility(View.GONE);
                        Log.e("tag", "" + response.toString());
                        try {
                            String status = response.getString("status");
                            String msg = response.getString("message");
                            if (status.equals("success")) {
                                showAlert(Cart.this);
                            } else {
                                Toast.makeText(Cart.this, "" + msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                progressBar.setVisibility(View.GONE);
                Toast.makeText(Cart.this, "Network Problem!", Toast.LENGTH_SHORT).show();
            }
        });
        Volley.newRequestQueue(this).add(req);
    }

    private void showAlert(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Thank You")
                .setMessage("Your booking has been scheduled.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finishAffinity();
                        Intent intent = new Intent(Cart.this, Home.class);
                        startActivity(intent);
                    }
                })
                .show();
    }

    public void addAddress(View view) {
        Constants.clickEffect(view);
        Intent intent = new Intent(this, Addresses.class);
        intent.putExtra("from", "Cart");
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                addressId = data.getIntExtra("address_id", 0);
                if (data.getStringExtra("address") != null) {
                    address.setVisibility(View.VISIBLE);
                    address.setText(data.getStringExtra("address"));
                }
            }
        }
    }

    public void headerClick(View view) {
        if (isExpanded) {
            Log.e("tag", "expanded");
            listView.setVisibility(View.GONE);
            arrow.setImageResource(R.drawable.arrow_down1);
        } else {
            Log.e("tag", "collapsed");
            listView.setVisibility(View.VISIBLE);
            arrow.setImageResource(R.drawable.up_arrow);
        }
        DrawableCompat.setTint(arrow.getDrawable(), ContextCompat.getColor(this, R.color.black));
        isExpanded = !isExpanded;
    }

    public void finish(View view) {
        Constants.clickEffect(view);
        finish();
    }

}
