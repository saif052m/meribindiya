package com.deecoders.meribindiya.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.deecoders.meribindiya.R;
import com.deecoders.meribindiya.activity.Addresses;
import com.deecoders.meribindiya.activity.Bookings;
import com.deecoders.meribindiya.activity.Cart;
import com.deecoders.meribindiya.activity.EditProfile;
import com.deecoders.meribindiya.activity.Otp;
import com.deecoders.meribindiya.constants.Constants;
import com.deecoders.meribindiya.util.MyPref;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainMenu extends Fragment {
    String type;
    @BindView(R.id.profileImg)
    ImageView profileImg;
    @BindView(R.id.profilePanel)
    LinearLayout profilePanel;
    @BindView(R.id.cartImg)
    ImageView cartImg;
    @BindView(R.id.cartPanel)
    LinearLayout cartPanel;
    @BindView(R.id.addressImg)
    ImageView addressImg;
    @BindView(R.id.addressPanel)
    LinearLayout addressPanel;
    @BindView(R.id.bookingImg)
    ImageView bookingImg;
    @BindView(R.id.bookingPanel)
    LinearLayout bookingPanel;
    Unbinder unbinder;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.logoutImg)
    ImageView logoutImg;
    @BindView(R.id.logoutPanel)
    LinearLayout logoutPanel;

    public static MainMenu newInstance(String type) {
        MainMenu fragment = new MainMenu();
        Bundle args = new Bundle();
        args.putString("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_menu, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        DrawableCompat.setTint(profileImg.getDrawable(), ContextCompat.getColor(getActivity(), R.color.gray));
        DrawableCompat.setTint(cartImg.getDrawable(), ContextCompat.getColor(getActivity(), R.color.gray));
        DrawableCompat.setTint(addressImg.getDrawable(), ContextCompat.getColor(getActivity(), R.color.gray));
        DrawableCompat.setTint(bookingImg.getDrawable(), ContextCompat.getColor(getActivity(), R.color.gray));
        DrawableCompat.setTint(logoutImg.getDrawable(), ContextCompat.getColor(getActivity(), R.color.gray));

        profilePanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.clickEffect(v);
                Intent intent = new Intent(getActivity(), EditProfile.class);
                startActivity(intent);
            }
        });
        addressPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.clickEffect(v);
                Intent intent = new Intent(getActivity(), Addresses.class);
                startActivity(intent);
            }
        });
        bookingPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.clickEffect(v);
                Intent intent = new Intent(getActivity(), Bookings.class);
                startActivity(intent);
            }
        });
        logoutPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.clickEffect(v);
                MyPref.setLogin(getActivity(), 0);
                getActivity().finishAffinity();
                Intent intent = new Intent(getActivity(), Otp.class);
                startActivity(intent);
            }
        });

        name.setText(MyPref.getName(getActivity()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
